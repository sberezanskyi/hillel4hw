import java.util.ArrayList;

public class Education {

    // Здесь все задачи из Google таблицы

    // -------------------------------------------------------------------

    // *** 1. Write a program which prints to the console some string 5 times. ***

//    public static void main(String[] args) {
//        printString(5);
//    }
//
//    public static void printString(int n) {
//        if (n > 0) {
//            System.out.print("\nsome string\n");
//            printString(n - 1);
//        }
//    }

    // -------------------------------------------------------------------

    // *** 2. Write a program which creates and initializes all primitive data types. ***

//    public static boolean a = true;
//    public static byte b = 100;
//    public static char c = '\u3072';
//    public static short d = 15000;
//    public static int e = 15900000;
//    public static long f = 7000000000l;
//    public static float g = 1.400798f;
//    public static double h = 1.790000033;
//
//    public static void main(String[] args) {
//
//
//        System.out.println("boolean "+ a);
//        System.out.println("byte " + b);
//        System.out.println("char " + c);
//        System.out.println("short "+d);
//        System.out.println("int "+e);
//        System.out.println("long "+f);
//        System.out.println("float "+g);
//        System.out.println("double "+h);
//
//    }

    // -------------------------------------------------------------------

    // *** 3. Create a program which calculates square of a rectangle. ***

//    public static void main(String[] args) {
//        int triangleSquare = square(5, 10);
//        System.out.println("Square of triangle is " + triangleSquare);
//    }
//
//    public static int square(int a, int b) {
//        int result = a * b;
//        return result;
//
//    }

    // -------------------------------------------------------------------

    // *** 4. Fix the program to get the correct result: ***

//    public static float a = 5;
//    public static float b = 10;
//    public static void main(String[] args) {
//
//        float result = a / b;
//
//        System.out.println(result);
//    }

    // -------------------------------------------------------------------

    // *** 5. Change the operations  to get the result 20: ***

//    public static int a = 1;
//    public static int b = 3;
//    public static int c = 9;
//    public static int d = 27;
//
//    public static void main(String[] args) {
//
//        //int result = - a * b / c + d  -- sample;
//        int result = - a + b - c + d;
//
//        System.out.println(result);
//
//    }

    // -------------------------------------------------------------------

    // *** 6. Comment redundant strings to get the output "Happy Java Learning": ***

//    public static void main(String[] args) {
////        String s = "Java";
////        System.out.println("Happy");
////        System.out.println("Java Learning");
////        System.out.println("programming");
//        System.out.print("Happy Java");
////        System.out.println("weekend");
////        System.out.println(s);
////        System.out.print("Python");
//        System.out.print(" ");
//        System.out.println("Learning");
//    }

    // -------------------------------------------------------------------

    // *** 7. Comment variables which are never used. ***

//    public static void main(String[] args) {
//        int a = 10;
//        int b = 15;
//        double c = b + 38;
//        //int d = a + 12;
//        //double e = 12.3;
//        String s = "s" + a;
//        String s1 = a + "b";
//        //String s2 = "a";
//        String s3 = s1 + "a";
//        String s4 = s3 + "b";
//        System.out.println(c + s4 + s);
//    }

    // -------------------------------------------------------------------

    // *** 8*. Write a program which prints "Hello world" to the console using methods transformIntToChar and printList only. ***

//    public static void main(String[] args) {
//        Education.printList(Education.transformIntToChar(72,101,108,108,111,32,119,111,114,108,100));
//    }
//
//    private static ArrayList<Character> transformIntToChar(int... values) {
//        ArrayList buffer = new ArrayList();
//        for (int i: values) {
//            buffer.add(((char) i));
//        }
//        return buffer;
//    }
//
//    private static void printList(ArrayList<Character> characters) {
//        for (char c: characters) {
//            System.out.print(c);
//        }
//    }



}