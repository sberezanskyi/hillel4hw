public class Loops {

    // Здесь все задачи из LMS, задачи из Google таблицы в классе Education

    //*** 1. Print numbers from 10 to 20 using all known cycles. ***

    public static void main(String[] args) {

        //while
        System.out.println("\nWhile loop");
        int x = 10;
        while (x <= 20) {
            System.out.print(x + " ");
            x++;
        }

        //do ... while
        System.out.println("\n\n'Do ... while' loop");
        int y = 10;
        do {
            System.out.print(y + " ");
            y++;
        } while (y <= 20);

        //for
        System.out.println("\n\nFor loop");
        int z;
        for (z = 10; z <= 20; z++) {
            System.out.print(z + " ");
        }

        //for each
        System.out.println("\n\n'For each' loop");
        int[] array = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        for (int i : array) {
            System.out.print(i + " ");
        }

        // ----------------------------------------------

        //*** 2. Print all the numbers from 1 to 100, which are divided by 3 without a remainder. ***

        System.out.println("\n\nRemainder");
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.print(i + " ");
            }
        }

        // ----------------------------------------------

        //*** 3. Write a function which calculates and returns the minimum of 4 arguments. ***

        int totalResult = multABCD(3, 10);
        System.out.println("\n\nMultiplication result: " + totalResult);
    }

    public static int multABCD(int c, int d) {
        int resultCD = c * d;
        return resultCD * multAB(5, 6);
    }

    public static int multAB(int a, int b) {
        int resultAB = a * b;
        return resultAB;
    }

}



